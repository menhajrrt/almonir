<html ng-app="quranApp">
<head>
  <title>المنیر</title>
  <meta charset="utf-8" />
  <meta name="description" content="قرائت صفحه به صفحه قرآن کریم با تلاوت قاریان برتر جهان اسلام">
  <meta name="author" content="Menhaj Service">
  <meta name="keywords" content="Quran,Islam,almonir,quran site,QURAN,المنیر,قرآن,قرآن کریم,ترتیل صفحه به صفحه,صفحه قرآن کریم,سوره,آیه,اسلام,شیعه,ایران,تلاوت صفحه به صفحه قرآن کریم">
  <meta name="viewport" content="width=device-width,initial-scale=1">
  <link type="image/x-icon" rel="shortcut icon" href="./images/Quran_fav.png">
  <link href="<?php echo "./css/style.css?v=".rand(1001, 9999) ?>" rel="stylesheet" type="text/css">
  <link href="./css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

  <script src="./js/angular.min.js"></script>
  <script src="./js/angular-cookies.min.js"></script>
  <script src="./js/jquery.min.js"></script>
  <script src="./js/persianumber.js"></script>
  <script src="./js/angular.js"></script>
</head>
<body>
  <div class="container">
    <div class="content">
      <?php include 'quran-page.php'; ?>
    </div>
  </div>

  <script src="<?php echo "./js/functions.js?v=".rand(1001, 9999) ?>"></script>
  <script src="<?php echo "./js/javascript.js?v=".rand(1001, 9999) ?>"></script>

  <div class="popup">
    <div class="popup-bg"></div>
    <div class="popup-content"></div>
  </div>

  <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-88097280-1', 'auto');
    ga('send', 'pageview');
  </script>

  <?php include './php/player.php'; ?>
</body>
</html>
