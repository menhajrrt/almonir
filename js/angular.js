/*
*   Angular Scope
*/
var quranApp = angular.module("quranApp", ["ngCookies"]);

Array.prototype.remove = function(element)
{
  var index = this.indexOf(element);
  this.splice(index, 1);
  return this;
}

/*
*   Bookmarks Controller
*/
quranApp.controller("bookmarksController", function($scope, $cookies)
{
  $scope.currentPage = function()
  {
    return window.currentQuranIndex();
  }

  $scope.getBookmarks = function(bookmarkArray)
  {
    if (!bookmarkArray)
    {
      var bookmarkCookie = $cookies.get('_bookmarks');

      if (bookmarkCookie != '')
      {
        bookmarkArray = bookmarkCookie.split('-');
      }
      else
      {
        return null;
      }
    }

    if (typeof bookmarkArray !== 'undefined' && bookmarkArray.length > 0)
    {
      var bookmarkJson = JSON.stringify(bookmarkArray);
      $scope.bookmarks = {
        'pages': []
      };

      angular.forEach(bookmarkArray, function(v, k) {
        if (k > 0)
        {
          $scope.bookmarks.pages.push({
            'page': v
          });
        }
      });

      return $scope.bookmarks.pages;
    }
    else
    {
      return false;
    }
  }

  $scope.reloadBookmarks = function()
  {
    $scope.bookmarks = $scope.getBookmarks();
  }

  $scope.removeBookmark = function(page)
  {
    var bookmarkArray = $cookies.get('_bookmarks').split('-');
    if (bookmarkArray.indexOf(page) > 0)
    {
      bookmarkArray.remove(page);
      var newBookmarks = bookmarkArray.join('-');
      window.createCookie('_bookmarks', newBookmarks, 365);
    }

    $scope.reloadBookmarks();
  }

  $scope.addBookmark = function()
  {
    var page = $scope.currentPage();
    var bookmarkArray = $cookies.get('_bookmarks').split('-');

    if (bookmarkArray.indexOf(page) < 0)
    {
      bookmarkArray.push(page);
      var newBookmarks = bookmarkArray.join('-');
      window.createCookie('_bookmarks', newBookmarks, 365);
    }

    $scope.reloadBookmarks();
  }
});
