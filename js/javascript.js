$(document).ready(function()
{
  /*
  *
  *   Default Variables
  */
  var audioPlayer = $('audio#quran-player')[0];
  $('body').persiaNumber();

 /*
  *
  *   Default Cookies
  */

  if (checkCookie('_setting') == false)
  {
    createCookie('_setting','setting-sort=justify-read=page-qari=parhizgar-autoplay=false-view=light', 365);
  }

  if (checkCookie('_tr') == false)
  {
    createCookie('_tr','mks', 365);
  }

  if (checkCookie('_bookmarks') == false)
  {
    createCookie('_bookmarks','', 365);
  }

  if (checkCookie('_lastpage') == false)
  {
    createCookie('_lastpage',currentQuranIndex(), 365);
  }

 /*
  *
  *   Preloads
  */
  if(window.location.hash)
  {
    var load_page = window.location.hash.split('#')[1];
  }
  else
  {
    var load_page = getCookie('_lastpage');
  }

  loadQuran(load_page);

  if (getSetting('autoplay') == 'true' && $(audioPlayer).attr('src') !== false)
  {
    playQuran(load_page);
  }
  else
  {
    var playType = getSetting('read');
    var currentPage = load_page;
    var audioPlayerSrc = quran_src(playType, currentPage);
    $(audioPlayer).attr('src', audioPlayerSrc);
  }

  /*
  *   Quran Functions
  */

  $('.play-quran').click(function()
  {
    if ($('audio#quran-player').attr('src') == '')
    {
      playQuran();
    }
    if(audioPlayer.paused)
    {
        $(this).addClass('paused');
        audioPlayer.play();
    }
    else
    {
        $(this).removeClass('paused');
        audioPlayer.pause();
    }
  });


  /*
  *   Html pages javascripts
  */
  $('span#currentPageNumberText span').html(load_page);

  $('.quran-page-selector').change(function ()
  {
      var index = parseInt($(this).val());
      if(index > 0 && index < 605)
      {
          loadQuran(index);
          if (getSetting('autoplay') == 'true' && $(audioPlayer).attr('src') !== false)
          {
            playQuran(index);
          }
          else
          {
            playQuran(index, false);
          }
      }

      $('span#currentPageNumberText span').html(index).show();
      $('span#currentPageNumberText input').hide();
  });

  $('.quran-page-navigator .next').click(function ()
  {
      var index = currentQuranIndex();
      if(parseInt(index)+1 != 605)
      {
        var nextIndex = parseInt(index)+1;
        $('.quran-page-selector').val(nextIndex);
        $('.quran-page-number span').html(nextIndex);
        $('span#currentPageNumberText span').html(nextIndex);

        loadQuran(nextIndex);

        if (getSetting('autoplay') == 'true' && $(audioPlayer).attr('src') !== false)
        {
          playQuran(nextIndex);
        }
        else
        {
          playQuran(nextIndex, false);
        }
      }
  });

  $('.quran-page-navigator .previous').click(function ()
  {
      var index = currentQuranIndex();
      if(parseInt(index)-1 != 0)
      {
          var previousIndex = parseInt(index-1);
          $('.quran-page-selector').val(previousIndex);
          $('.quran-page-number span').html(previousIndex);
          $('span#currentPageNumberText span').html(previousIndex);

          loadQuran(previousIndex);

          if (getSetting('autoplay') == 'true' && $(audioPlayer).attr('src') !== false)
          {
            playQuran(previousIndex);
          }
          else
          {
            playQuran(previousIndex, false);
          }
      }
  });

  if ($(window).width() > 768)
  {
    $('span#currentPageNumberText').click(function()
    {
      $(this).children('span').hide();
      $(this).children('input').show();
    });

  }
  else
  {
    $('span#currentPageNumberText').click(function()
    {
      var page = prompt("شماره صفحه:", currentQuranIndex());
      if(page)
      {
        if (page != null && page > 1 && page < 604)
        {
          loadQuran(page);
          if (getSetting('autoplay') == 'true' && $(audioPlayer).attr('src') !== false)
          {
            playQuran(page);
          }
          else
          {
            playQuran(page,false);
          }
        }
        else
        {
          alert(' صفحه مورد نظر غیر معتبر می باشد.');
        }
      }
    });

  }

  /*
  *   Audio Player functions
  */
  $('.audio-control').click(function()
  {
    $('a#audio-download-link').attr('href', audioPlayer.src);
    if(audioPlayer.paused)
    {
      audioPlayer.play();
      $(this).removeClass('fa-play').addClass('fa-pause');
    }
    else
    {
      audioPlayer.pause();
      $(this).removeClass('fa-pause').addClass('fa-play');
    }
  });


  $('.audio-stop').click(function()
  {
    var song = $('#quran-player')[0];
    $('a#audio-download-link').attr('href', song.src);
    song.pause();
    song.currentTime = 0;
    if($('.audio-control').hasClass('fa-pause'))
    {
        $('.audio-control').removeClass('fa-pause').addClass('fa-play');
    }
  });

  /*
  *   Main menu codes
  */
  $('ul.nav li, .quran-page .content').click(function()
  {
    $('ul.child').hide();
    $('ul.nav li.hasChild').removeClass('childOpen');
  });

  $('ul.nav li.hasChild').click(function()
  {
    $('ul.child').hide();
    $('ul.nav li.hasChild').removeClass('childOpen');
    if ($(this).hasClass('childOpen'))
    {
      $(this).children('ul.child').hide();
      $(this).removeClass('childOpen');
    }
    else
    {
      $(this).children('ul.child').show();
      $(this).addClass('childOpen');
    }
  });

  $('ul.nav li.hasPopup').click(function()
  {
    var page = $(this).children('a').attr('data-load');
    var width = $(this).children('a').attr('data-width');
    var param = $(this).children('a').attr('data-param');

    if (!width) {
      width = '45%';
    }

    $.get('./php/'+page+'.php?param='+param, function(data)
    {
      $.getScript('./js/innerJs.js');
      $('.popup').show();
      $('.popup-content').css('width', width);
      $('.popup-content').html(data);
    });
  });

  $('ul.nav li.hasDropdown').click(function()
  {
    var page = $(this).children('a').attr('data-load');

    $.get('./php/'+page+'.php', function(data)
    {
      $.getScript('./js/innerJs.js');
      $('.setting-nav').html(data);
      $('.setting-nav').slideDown();
    });
  });

  $('.fa.check-tr').hide();
  var current_tr = getCookie('_tr');
  $("a[data-bind='"+current_tr+"']").children('.check-tr').show();

  $("a[data-role='tr']").click(function()
  {
    var tr = $(this).attr('data-bind');
    var currentIndex = currentQuranIndex();
    $('.fa.check-tr').hide();
    $(this).children(".check-tr").show();
    loadQuran(currentIndex, tr);
  });

  // Responsive functions
  $('.menu .menu-icon').click(function() {
    $('.menu .menu-icon i').toggleClass('fa-bars');
    $('.menu .menu-icon i').toggleClass('fa-remove');
    $('ul.nav').toggle();
  });

  if ($(window).width() < 1000)
  {
    $("ul.nav li.hasPopup a[data-load='index']").attr('data-param','mobile').attr('data-width','50%');
  }

  window.addEventListener("resize", function()
  {
    if ($(window).width() < 1000)
    {
      $("ul.nav li.hasPopup a[data-load='index']").attr('data-param','mobile').attr('data-width','50%');
    }
    else
    {
      $("ul.nav li.hasPopup a[data-load='index']").attr('data-param','desktop');
      $('ul.nav').show();
    }
  });


  /*
  *   Pop up functions
  */
  $('.popup-bg').click(function()
  {
    $('.popup').hide();
    $('.popup-content').html();
  });

  /*
  *   Bookmark functions
  */
  var sidebarWidth = $('.quran-page .sidebar').outerWidth();

  $('.quran-page .sidebar').css('left','-'+sidebarWidth);

  $('.fa.bookmark').click(function()
  {
    $('.quran-page .sidebar').show();
    $('.quran-page .sidebar').animate({'left':'0'}, 500);
    //loadBookmarks();
    $('.setting-nav').slideUp();
  });

  $('.quran-page .content').click(function()
  {
    $('.quran-page .sidebar').animate({'left':'-'+sidebarWidth}, 500, function() {
      $('.quran-page .sidebar').hide();
    });
    $('.setting-nav').slideUp();
  });

  $(window).scroll(function()
  {
    if ($(this).width() > 768)
    {
      if ($(this).scrollTop() >= 60)
      {
        $('.menu').addClass('fixed');
        $('.menu .quran-page-navigator.menubar').show();
        $('.sidebar').css('padding-top', '40px');
        $('.quran_textbox').css('margin-top', 100-$(this).scrollTop());
      }
      else
      {
        $('.menu').removeClass('fixed');
        $('.menu .quran-page-navigator.menubar').hide();
        $('.sidebar').css('padding-top', 100-$(this).scrollTop());
        $('.quran_textbox').css('margin-top', '0px');
      }

    }
    else
    {
      if ($(this).scrollTop() >= 55)
      {
        $('.menu').addClass('fixed');
        $('.menu .quran-page-navigator.menubar').show();
        $('.sidebar').css('padding-top', '40px');
        $('.quran_textbox').css('margin-top', 100-$(this).scrollTop());
      }
      else
      {
        $('.menu').removeClass('fixed');
        $('.menu .quran-page-navigator.menubar').hide();
        $('.sidebar').css('padding-top', 100-$(this).scrollTop());
        $('.quran_textbox').css('margin-top', '10px');
      }
    }

  });

  /*
  *   Setting javascripts
  */

  if(!checkSetting('read'))
  {
    setSetting('read', 'page');
  }

  if(!checkSetting('qari'))
  {
    setSetting('qari', 'parhizgar');
  }

  if(!checkSetting('autoplay'))
  {
    setSetting('autoplay', 'false');
  }

  if(!checkSetting('sort'))
  {
    setSetting('sort', 'right');
  }

  if(!checkSetting('view'))
  {
    setSetting('view', 'day');
  }

  setPageSortSetting();
  setPageReadSetting();
  setPageAutoplaySetting();

  $('select#selectQari').val(getSetting('qari'));

  $('.fa.setting').click(function()
  {
    $('.setting-nav').slideToggle();
  });

  $('.sort-text').click(function(ev)
  {
    ev.preventDefault();

    var sort = $(this).attr('data-bind');
    setSetting('sort', sort);
    loadQuran();
    $('.sort-text').removeClass('active');
    $(this).addClass('active');
  });

  $('.read-text').click(function(ev)
  {
    ev.preventDefault();

    var read = $(this).attr('data-bind');
    setSetting('read', read);
    playQuran();
    $('.read-text').removeClass('active');
    $(this).addClass('active');
  });

  $('select#selectQari').change(function ()
  {
      var qari = $(this).val();
      setSetting('qari', qari);

      if (getSetting('autoplay') == 'true' && $(audioPlayer).attr('src') !== false)
      {
        playQuran();
      }
      else
      {
        playQuran(null,false);
      }
  });

  $('input#autoplayAudio').change(function()
  {
    if ($(this).is(':checked'))
    {
      setSetting('autoplay', 'true');
      playQuran();
    }
    else
    {
      setSetting('autoplay', 'false');
      playQuran();
    }
  });

  $('.settingView').click(function(ev) {
    ev.preventDefault();
    $('.settingView').removeClass('active');
    $(this).addClass('active');

    if ($(this).hasClass('light'))
    {
      setSetting('view', 'light');
      loadDarknessCSS(true);
    }
    else
    {
      setSetting('view', 'darkness');
      loadDarknessCSS(false);
    }
  });

  if (getSetting('view') == 'light')
  {
    $('.settingView').removeClass('active');
    $('.settingView.light').addClass('active');
    loadDarknessCSS(true);
  }

  if (getSetting('view') == 'darkness')
  {
    $('.settingView').removeClass('active');
    $('.settingView.darkness').addClass('active');
    loadDarknessCSS(false);
  }

  /*
  *   Player javascripts
  */
  $('a#audio-download-link').attr('href', audioPlayer.src);

  audioPlayer.addEventListener("timeupdate", function() {
    var currentTime = audioPlayer.currentTime;
    var duration = audioPlayer.duration;

    var width = (currentTime/duration)*100;

    $('.playerArea .station .audio-duration .audio-duration-progress').width(width+'%');
    $('.playerArea .station .audio-duration .audio-duration-text .audio-duration-text-current').html(formatPlayerTimeString(currentTime));
    $('.playerArea .station .audio-duration .audio-duration-text .audio-duration-text-total').html(formatPlayerTimeString(duration));
  });

  $('.playerArea .station .audio-duration').click(function(e)
  {
    var durationWidth = $(this).width();
    var offset = $(this).offset();
    var relativeX = (e.pageX - offset.left);
    var audioDuration = audioPlayer.duration;

    percent = relativeX/durationWidth;
    $(this).children('.audio-duration-progress').width((percent*100)+'%');

    var selectedTime = audioDuration*percent;
    audioPlayer.currentTime = selectedTime.toFixed(2);
  });

  $('.audio-volume').click(function()
  {
    $('.audio-volume-panel').fadeIn();
  });

  $('.audio-volume-panel').on('mouseleave', function()
  {
    $('.audio-volume-panel').fadeOut();
  });

  $('.container').click(function()
  {
    $('.audio-volume-panel').fadeOut();
  });

  $('.audio-volume-panel').click(function(e)
  {
    var volumeWidth = $(this).height();
    var offset = $(this).offset();
    var relativeY = (e.pageY - offset.top);

    decimal = relativeY/volumeWidth;

    if (decimal > 0.9)
    {
      percent = '90';
    }
    else
    {
      percent = decimal*100;
    }
    $(this).children('.audio-volume-progress').height(percent+'%');

    audioPlayer.volume = decimal.toFixed(2);
  });

  $('.audio-repeat').click(function()
  {
    if($(this).hasClass('active'))
    {
      $(this).removeClass('active');
    }
    else
    {
      $(this).addClass('active');
    }
  });

  audioPlayer.addEventListener('ended', function()
  {
      if ($('.audio-repeat').hasClass('active'))
      {
        this.currentTime = 0;
        this.play();
        $('.audio-control').removeClass('fa-play').addClass('fa-pause');
      }
      else
      {
        $('.audio-control').removeClass('fa-pause').addClass('fa-play');
      }


      if (getSetting('autoplay') == 'true' && !$('.audio-repeat').hasClass('active'))
      {
        var index = currentQuranIndex();
        if(parseInt(index)+1 != 605)
        {
          var nextIndex = parseInt(index);
          $('.quran-page-selector').val(nextIndex+1);
          loadQuran(nextIndex+1);
          playQuran(nextIndex+1);
          $('.quran-page-number span').html(nextIndex+1);
        }
      }
    }, false);

});
