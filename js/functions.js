$(document).ready(function()
{
  /*
  *   Global variables
  */
  var entireQuranPage;
  var pageFirstAya;
  var pageLastAya;

  /*
  *   Quran mp3 files directory
  */
  var page_reciters = {
    "parhizgar":"http://www.everyayah.com/data/Parhizgar_48kbps/PageMp3s/",
    "alafasi":"http://www.everyayah.com/data/Alafasy_64kbps/PageMp3s/",
    "shateri":"http://www.everyayah.com/data/Abu_Bakr_Ash-Shaatree_64kbps/PageMp3s/",
    "abdolbasit":"http://www.everyayah.com/data/Abdul_Basit_Murattal_192kbps/PageMp3s/",
    "menshavi":"http://www.everyayah.com/data/Menshawi_16kbps/PageMp3s/",
    "tablaway":"http://www.everyayah.com/data/Mohammad_al_Tablaway_128kbps/PageMp3s/",
    "rifaie":"http://www.everyayah.com/data/Hani_Rifai_64kbps/PageMp3s/",
    // Translations
    "fa_makarem":"http://www.everyayah.com/data/translations/Makarem_Kabiri_16Kbps/PageMp3s/",
  };

  /*
  Feature for Future

  var aya_reciters = {
    "parhizgar":"http://www.everyayah.com/data/Parhizgar_48kbps/",
    "alafasi":"http://www.everyayah.com/data/Alafasy_64kbps/",
    "shateri":"http://www.everyayah.com/data/Abu_Bakr_Ash-Shaatree_64kbps/",
    "abdolbasit":"http://www.everyayah.com/data/Abdul_Basit_Murattal_192kbps/",
    "menshavi":"http://www.everyayah.com/data/Menshawi_16kbps/",
    "tablaway":"http://www.everyayah.com/data/Mohammad_al_Tablaway_128kbps/",
    // Translations
    "fa_makarem":"http://www.everyayah.com/data/translations/Makarem_Kabiri_16Kbps/PageMp3s/",
  };
  */

  /*
  *   Cookies Functions
  */
  window.createCookie = function(name,value,days)
  {
    if (days)
    {
        var date = new Date();
        date.setTime(date.getTime()+(days*24*60*60*1000));
        var expires = "; expires="+date.toGMTString();
    }
    else var expires = "";
    document.cookie = name+"="+value+expires+"; path=/";
  }

  window.getCookie = function(name)
  {
      var nameEQ = name + "=";
      var ca = document.cookie.split(';');
      for(var i=0;i < ca.length;i++)
      {
          var c = ca[i];
          while (c.charAt(0)==' ') c = c.substring(1,c.length);
          if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
      }
      return null;
  }

  window.deleteCookie = function(name)
  {
      createCookie(name,"",-1);
  }

  window.checkCookie = function(name)
  {
    if(document.cookie.indexOf(name) == -1)
    {
      return false;
    }
    else
    {
      return true;
    }
  }


  /*
  *   Quran Functions
  */


  window.currentQuranIndex = function()
  {
    return $('input.quran-page-selector').val();
  }

  window.loadQuran = function(index, tr)
  {
      if (!tr)
      {
        if (checkCookie('_tr') != false)
        {
          tr = getCookie('_tr');
        }
        else
        {
          tr = 'mks';
        }
      }
      deleteCookie('_tr');
      createCookie('_tr', tr);

      var url;
      if (getSetting('sort') == 'right')
      {
        urlAddress = './php/quran-content.php';
      }
      else
      {
        urlAddress = './php/quran-content-justify.php';
      }

      if (!index)
      {
        index = currentQuranIndex();
      }

      $.ajax({
        url: urlAddress,
        type: 'post',
        data: {'index':index, 'tr': tr},
        beforeSend: function()
        {
          $('.quran-page .content').css('opacity','0.4');
          $('.quran-page .content').addClass('noselect');
        },
        success: function(data)
        {
          $('.quran-page .content').css('opacity','1');
          $('.quran-page .content').removeClass('noselect');
          $('.quran-page .content').html(data);
          $('#download-track').attr('href',quran_src(index));
          $('.quran-page-number span').html(index);
          $('.quran-page-selector').val(index);
          $('span#currentPageNumberText span').html(index);

          createCookie('_lastpage',index, 365);
          $.getScript('./js/innerJs.js');

          document.title = 'المنیر'+'-'+'قرآن کریم صفحه '+index;
          window.location.hash = "#"+index;
        }
      })
  }

  window.quran_src = function(playType, page)
  {
    var qari = '';

    if (playType == 'page')
    {
      if(page < 10)
      {
          var track = '00'+page;
      }
      if(page < 100 && page > 10)
      {
          track = '0'+page;
      }
      if(page > 100)
      {
          track = page;
      }

      qari = getSetting('qari');
      return page_reciters[qari]+'Page'+track+'.mp3';
    }

    if (playType == 'aya')
    {
      var suraNum = page.split(':')[0];
      var ayaNum = page.split(':')[1];

      if(suraNum < 10)
      {
          suraNum = '00'+suraNum;
      }
      if(suraNum < 100 && suraNum > 10)
      {
          suraNum = '0'+suraNum;
      }
      if(suraNum > 100)
      {
          suraNum = suraNum;
      }

      if(ayaNum < 10)
      {
          ayaNum = '00'+ayaNum;
      }
      if(ayaNum < 100 && ayaNum > 10)
      {
          ayaNum = '0'+ayaNum;
      }
      if(ayaNum > 100)
      {
          ayaNum = ayaNum;
      }

      qari = getSetting('qari');

      return aya_reciters[qari]+suraNum+ayaNum+'.mp3';
    }

  }

  window.playQuran = function(page, autoplay = true, aya = '')
  {
    var pageNum = page;
    var ayaNum = aya;

    var playType = getSetting('read');

    if (!page)
    {
      page = currentQuranIndex();
    }

    if (!aya && playType == 'aya')
    {
      ayaNum = pageFirstAya;
    }

    if (playType == 'aya')
    {
      var src = quran_src(playType, aya);

      $('audio#quran-player').attr('src', src);

      if (autoplay === true)
      {
        $('audio#quran-player').attr('autoplay','autoplay');
        $('.audio-control').removeClass('fa-play').addClass('fa-pause');
      }

      $('a#audio-download-link').attr('href', src);

      $('audio#quran-player')[0].addEventListener("ended", function ()
      {
        if ($('.audio-repeat').hasClass('active'))
        {
          this.currentTime = 0;
          this.play();
        }
        else
        {
          $(this).currentTime = 0;
          $('.audio-control').removeClass('fa-pause').addClass('fa-play');
        }
      }, false);
    }
    else
    {
      var src = quran_src(playType, page);

      $('audio#quran-player').attr('src', src);

      if (autoplay === true)
      {
        $('audio#quran-player').attr('autoplay','autoplay');
        $('.audio-control').removeClass('fa-play').addClass('fa-pause');
      }

      $('a#audio-download-link').attr('href', src);

      $('audio#quran-player')[0].addEventListener("ended", function ()
      {
        if ($('.audio-repeat').hasClass('active'))
        {
          this.currentTime = 0;
          this.play();
        }
        else
        {
          $(this).currentTime = 0;
          $('.audio-control').removeClass('fa-pause').addClass('fa-play');
        }
      }, false);
    }
  }

  window.indexQuran = function(handleData)
  {
    $.get('./php/quran-index.php', function(data) {
       var temp = JSON.parse(data);
       handleData(temp.quran_page);
    });
  };

  /*
  *   Setting javascripts
  */

  window.getSetting = function(item)
  {
    var settingCookie = getCookie('_setting');
    var settingElements = settingCookie.split('-');

    for (var i = 0; i < settingElements.length; i++)
    {
      if (settingElements[i] != 'setting')
      {
        var settingKey = settingElements[i].split('=')[0];
        var settingValue = settingElements[i].split('=')[1];

        if (settingKey == item)
        {
            return settingValue;
        }
      }
    }
  };

  window.setSetting = function(key, value)
  {
    var settingCookie = getCookie('_setting');
    var settingElements = settingCookie.split('-');
    var settingArray = ['setting'];
    var settingString = '';
    var item = '';

    for (var i = 0; i < settingElements.length; i++)
    {
      if (settingElements[i] != 'setting')
      {
        var settingKey = settingElements[i].split('=')[0];
        var settingValue = settingElements[i].split('=')[1];
        var itemArray = [];

        itemArray.push(settingKey);

        if (settingKey == key)
        {
          itemArray.push(value);
        }
        else
        {
          itemArray.push(settingValue);
        }

        item = itemArray.join('=');
        settingArray.push(item);
      }
    }

    settingString = settingArray.join('-');
    createCookie('_setting', settingString, 365);
  };

  window.checkSetting = function(name)
  {
    var settingCookie = getCookie('_setting');
    var settingElements = settingCookie.split('-');

    for (var i = 1; i < settingElements.length; i++)
    {
      var settingName = settingElements[i].split('=')[0];
      if (settingName == name)
      {
        return true;
      }
    }
    return false;
  }

  window.setPageSortSetting = function()
  {
    var pageSort = getSetting('sort');
    $(".sort-text").removeClass('active');
    $(".sort-text[data-bind='"+pageSort+"']").addClass('active');
  };

  window.setPageReadSetting = function()
  {
    var pageRead = getSetting('read');
    $(".read-text").removeClass('active');
    $(".read-text[data-bind='"+pageRead+"']").addClass('active');
  };

  window.setPageAutoplaySetting = function()
  {
    var autoplay = getSetting('autoplay');
    if (autoplay == 'true')
    {
      $('input#autoplayAudio').attr('checked', 'checked');
    }
    else
    {
      $('input#autoplayAudio').removeAttr('checked');
    }
  };

  window.formatPlayerTimeString = function(secs)
  {
    var hr  = Math.floor(secs / 3600);
    var min = Math.floor((secs - (hr * 3600))/60);
    var sec = Math.floor(secs - (hr * 3600) -  (min * 60));

    if (min < 10){
      min = "0" + min;
    }
    if (sec < 10){
      sec  = "0" + sec;
    }

    return min + ':' + sec;
  }

  window.loadDarknessCSS = function(remove = false)
  {
    if (!document.getElementById('darknessCss') && !remove)
    {
        var head  = document.getElementsByTagName('head')[0];
        var link  = document.createElement('link');
        link.id   = 'darknessCss';
        link.rel  = 'stylesheet';
        link.type = 'text/css';
        link.href = './css/darkness.style.css';
        link.media = 'all';
        head.appendChild(link);
    }
    else
    {
      $('link#darknessCss').remove();
    }
  };



  /*
  *   Tafsir javascripts
  */
  window.loadTafsir = function(aya_index=1)
  {
    var tafsir_address = "http://zekr.tebyan.net/Webservices/Maref/Quran_v2.asmx/GetTafsir";
    var objectData =
         {
             'AyehID': aya_index,
             'Tafsir':'5'
         };

     var objectDataString = JSON.stringify(objectData);

    $.ajax({
      url:tafsir_address,
      type:'POST',
      contentType:'application/json; charset=UTF-8', // This is the money shot
      dataType: "json",
      data:objectDataString,
      beforeSend: function() {
        console.log('sending ...');
      },
      success: function(data){
        console.log($(data));
      },
      error: function(xhr, status, error) {
        var err = eval(xhr.responseText);
        console.log(err);
      }
    });
  }


});
