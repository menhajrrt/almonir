$(document).ready(function()
{
  /*
  *
  *   Default Variables
  */
  var audioPlayer = $('audio#quran-player')[0];

  /*
  *  Bookmark functions
  */

  Array.prototype.remove = function(el)
  {
    return this.splice(this.indexOf(el), 1);
  }

  $(".wrap .bookmark .bookmark-text").each(function()
  {
    $(this).click(function(ev)
    {
      ev.preventDefault();
      var bookmarked_index = $(this).parent().attr('data-bind');
      loadQuran(bookmarked_index);

      if (getSetting('autoplay') == 'true' && $(audioPlayer).attr('src') !== false)
      {
        playQuran(bookmarked_index);
      }
      else
      {
        playQuran(bookmarked_index, false);
      }
    });
  });


  /*
  *   Quran index functions
  */
  $('ul.quran-index-ul li.quran-index-li').each(function()
  {
    $(this).click(function(ev)
    {
      ev.preventDefault();
      var index = $(this).attr('data-page');
      loadQuran(index);

      if (getSetting('autoplay') == 'true' && $(audioPlayer).attr('src') !== false)
      {
        playQuran(index);
      }
      else
      {
        playQuran(index,false);
      }

      $('.popup').hide();
    });
  });

  $('select.quran-index-select').change(function()
  {
    var index = parseInt($(this).val());
    loadQuran(index);

    if (getSetting('autoplay') == 'true' && $(audioPlayer).attr('src') !== false)
    {
      playQuran(index);
    }
    else
    {
      playQuran(index,false);
    }

    $('.popup').hide();
  });


  $('.quran_textbox .quran_ar_text span.quran-text-ar').click(function(ev)
  {
    ev.preventDefault();
    $('.quran_textbox .quran_ar_text span.quran-text-ar').removeClass('selected');
    $(".quran_textbox .quran_tr_text span.quran-text-tr").removeClass('selected');

    var aya_info = $(this).attr('data-aya');
    var aya_index = $(this).attr('data-index');

    $(this).addClass('selected');
    $(".quran_textbox .quran_tr_text span.quran-text-tr[data-aya='"+aya_info+"']").addClass('selected');
    loadTafsir(aya_index);
  });


  $('.quran_textbox .quran_tr_text span.quran-text-tr').click(function(ev)
  {
    ev.preventDefault();
    $('.quran_textbox .quran_ar_text span.quran-text-ar').removeClass('selected');
    $(".quran_textbox .quran_tr_text span.quran-text-tr").removeClass('selected');

    var aya_info = $(this).attr('data-aya');

    $(this).addClass('selected');
    $(".quran_textbox .quran_ar_text span.quran-text-ar[data-aya='"+aya_info+"']").addClass('selected');
  });

  /*
  $('.quran_textbox .quran_ar_text span.quran-text-ar small').click(function(ev)
  {
    ev.preventDefault();
    var aya_info = $(this).parent().attr('data-aya');
    alert(aya_info);
  });
  */

});
