<div class="menu noselect">
  <div class="pull-right">
    <span class="menu-icon">
      <i class="fa fa-bars fa-lg"></i>
    </span>
    <ul class="nav">

      <li class="hasPopup">
        <a data-load="index" data-width="70%" data-param="desktop">
          <i class="fa fa-list"></i>
          فهرست
        </a>
      </li>

      <li class="hasChild">
        <a>
          <i class="fa fa-language"></i>
          انتخاب ترجمه
        </a>
        <ul class="child">
          <li>
            <a data-bind="mks" data-role="tr">
              آیت الله مکارم شیرازی
              <i class="fa fa-check check-tr"></i>
            </a>
          </li>
          <li>
            <a data-bind="elq" data-role="tr">
              استاد الهی قمشه ای
              <i class="fa fa-check check-tr"></i>
            </a>
          </li>
          <li>
            <a data-bind="brp" data-role="tr">
              استاد بهرام پور
              <i class="fa fa-check check-tr"></i>
            </a>
          </li>
          <li>
            <a data-bind="fldv" data-role="tr">
              استاد فولادوند
              <i class="fa fa-check check-tr"></i>
            </a>
          </li>
        </ul>
      </li>
      <li class="hasPopup">
        <a data-load="about">
          <i class="fa fa-info"></i>
          درباره ما
        </a>
      </li>
      <li>
        <a href="http://payping.ir/menhajservice" target="_blank">
          <i class="fa fa-gift fa-lg" style="color:#71BA51"></i>
          <font color="#71BA51">همیاری</font>
        </a>
      </li>
    </ul>
  </div>

  <div class="pull-left left-menu">
      <i class="fa fa-bookmark fa-lg bookmark" title="افزودن به علاقه مندی ها"></i>
      <i class="fa fa-cog fa-lg setting" title="تنظیمات"></i>
  </div>

  <div class="pull-left noselect quran-page-navigator-area">
      <div class="quran-page-navigator menubar">
          <div class="pull-left next" title="صفحه بعد"><i class="fa fa-caret-left"></i></div>
          <span class='pull-left' id="currentPageNumberText">
            <span></span>
            <input type='number' min='1' max='604' class='quran-page-selector'>
          </span>
          <div class="pull-left previous" title="صفحه قبل"><i class="fa fa-caret-right"></i></div>
          <div class="clearfix"></div>
      </div>
  </div>
  <div class="clearfix"></div>

  <?php include './php/setting.php'; ?>
</div>
