<div class="quran-page">
    <div class="title noselect">
        <img src="./images/Quran_logo.png" class="quran-logo"/>
        <div class="pull-right quran-page-number" id="quran-page-number">
          قرآن المنیر -
            صفحه&nbsp;<span>1</span>
        </div>
        <div class="pull-left quran-page-counter">
            <span>قرآن کریم صفحه :</span>
            <input type="number" min="1" max="604" class="quran-page-selector" value="1">
        </div>

        <div class="pull-left noselect quran-page-navigator-area">
            <div class="quran-page-navigator">
                <div class="pull-left next" title="صفحه بعد"> > </div>
                <div class="pull-left previous" title="صفحه قبل"> < </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>

    <?php include 'menu.php'; ?>

    <div class="content" id="quran-content">
        <!-- load with Ajax -->
    </div>

    <div class="sidebar noselect" ng-controller="bookmarksController">
      <div class='header' ng-click="addBookmark()">
        <i class='fa fa-bookmark-o'></i>
        نشانه گذاری صفحه جاری
      </div>
      <div class='wrap' id="sidebar-wrap" ng-init="bookmarks = getBookmarks()">
        <div class='bookmark' ng-repeat="bookmark in bookmarks track by $index" data-bind='{{ bookmark.page }}' id="bookmark-{{ bookmark.page }}">
          <i class='fa fa-bookmark'></i>
          <span class='bookmark-text'>
          صفحه
          {{ bookmark.page }}
          </span>
          <div class='pull-left'>
            <i class='fa fa-trash bookmark-remove' ng-click="removeBookmark(bookmark.page)"></i>
          </div>
          <div class='clearfix'></div>
        </div>
        <div class='empty'>
            با حذف کوکی لیست صفحات از بین خواهد رفت.
        </div>
      </div>
    </div>

</div>
