<?php
if(!isset($_SESSION)){
    session_start();
}
$db_local = "localhost";
$db_name = "quran";
$db_user = "root";
$db_passwd = "";
$db_con = new PDO('mysql:host='.$db_local.';dbname='.$db_name.'',$db_user,$db_passwd);
$db_con->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
$db_con->exec("SET character_set_results = 'utf8',character_set_client = 'utf8',
				character_set_connection = 'utf8',character_set_database = 'utf8',
				character_set_server = 'utf8'");

if(!function_exists("quote_smart"))
{
	function quote_smart($value)
	{
		if(!is_numeric($value))
		{
			if(get_magic_quotes_gpc()) $value = stripslashes($value);
			return "'" .mysql_real_escape_string($value) . "'";
		}
		else
		{
			return $value;
		}

	}
}
if(!defined('HOME'))
{
    $homeUrl = 'http://'.$_SERVER['SERVER_NAME'].'/'.basename(__DIR__);
    define('HOME',$homeUrl);
}

?>
