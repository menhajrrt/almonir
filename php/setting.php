<div class="setting-nav">
  <ul class='setting-ul noselect'>
    <li>
      نحوه نمایش:
      <i class="fa fa-align-right sort-text" data-bind="right" title="جدا"></i>
      <i class="fa fa-align-justify sort-text" data-bind="justify" title="متصل"></i>
    </li>
    <!--
    <li>
      نحوه قرائت:
      <i class="fa fa-file-text-o read-text" data-bind="page" title="کل صفحه"></i>
      <i class="fa fa-indent read-text" data-bind="aya" title="آیه به آیه"></i>
    </li>
    -->
    <li>
      قاری:
      <select id="selectQari">
        <optgroup label="قرائت">
          <option value="parhizgar">پرهیزگار</option>
          <option value="menshavi">منشاوی</option>
          <option value="abdolbasit">عبدالباسط</option>
          <option value="alafasi">العفاسی</option>
          <option value="shateri">شاطری</option>
          <option value="tablaway">طبلاوی</option>
          <option value="rifaie">الرفاعی</option>
        </optgroup>
        <optgroup label="ترجمه">
          <option value="fa_makarem"> آیت الله مکارم شیرازی</option>
        </optgroup>
      </select>
    </li>

    <li>
      <label for="autoplayAudio">
        پخش اتوماتیک:
      </label>
      <input id="autoplayAudio" type="checkbox" value="true"/>
    </li>

    <li>
      <label>
        نوع نمایش:
      </label>
      <div class="settingView light">ب</div>
      <div class="settingView darkness">ب</div>
    </li>

  </ul>
</div>
