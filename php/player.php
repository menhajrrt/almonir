<div class="playerArea noselect">
  <audio id="quran-player"></audio>
  <div class="station">
    <i class="fa fa-play audio-control"></i>
    <i class="fa fa-stop audio-stop"></i>
    <div class="audio-duration">
      <div class="audio-duration-progress">
        <div class="audio-duration-sign"></div>
      </div>
      <div class="audio-duration-text">
        <div class="audio-duration-text-current"></div>
        <div class="audio-duration-text-total"></div>
      </div>
    </div>
    <i class="fa fa-volume-up audio-volume">
      <div class="audio-volume-panel">
        <div class="audio-volume-progress"></div>
      </div>
    </i>
    <i class="fa fa-repeat audio-repeat" title="تکرار"></i>

    <a href="" id="audio-download-link" target="_blank">
      <i class="fa fa-download audio-download" title="دریافت صوت قرائت صفحه"></i>
    </a>

  </div>
</div>
