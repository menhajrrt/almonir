<?php
    if(isset($_POST['index']) && isset($_POST['tr']))
    {
        if(!empty($_POST['index']) && is_numeric($_POST['index']) && intval($_POST['index']) < 605)
        {
            switch ($_POST['tr']) {
              case 'mks':
                $translator = 'fa_makarem';
                break;
              case 'elq':
                $translator = 'fa_ghomshei';
                break;
              case 'brp':
                $translator = 'fa_bahrampour';
                break;
              case 'fldv':
                $translator = 'fa_fooladvand';
                break;

              default:
                $translator = 'fa_makarem';
                break;
            }

            include '../class/quran-class.php';

            $index = intval($_POST['index']);

            $quranClass = new Quran;
            $quranClass->getIndex($index);

            $quran_suras = $quranClass->getAllSuras();

            $topPage = $quranClass->getDatas($index);
            $pageTopSura = $topPage[0]['quran_pages_page_sura'];
            $pageTopAya = $topPage[0]['quran_pages_page_aya'];

            if($index != 604)
            {
                $bottomPage = $quranClass->getDatas($index+1);
                $pageBottomSura = $bottomPage[0]['quran_pages_page_sura'];
                $pageBottomAya = $bottomPage[0]['quran_pages_page_aya'];
            }
            else
            {
                $pageBottomSura = 114;
                $pageBottomAya = 6;
            }

            $quran_page_first = $quranClass->getTexts($pageTopSura, $pageTopAya);
            $quran_page_end = $quranClass->getTexts($pageBottomSura, $pageBottomAya);

            $quran_page_first_index = $quran_page_first[0]['index'];
            $quran_page_end_index = $quran_page_end[0]['index']-1;

            $quran_page_ar = $quranClass->getTextsPage($quran_page_first_index, $quran_page_end_index);
            $quran_page_tr = $quranClass->getTextsPageTr($translator, $quran_page_first_index, $quran_page_end_index);

            echo "<div class='quran_textbox'><div class='quran_ar_text pull-right'>";
            echo "<div class='quran_textbox_info'><div class='pull-right'>".$quran_suras[$quran_page_ar[0]['sura']-1]['quran_suras_sura_name']."</div>";
            echo    "<div class='pull-left'>".$index."</div><div class='clearfix'></div></div>";
            echo "<div class='quran_ar_content'>";

            foreach($quran_page_ar as $aya_ar)
            {
                foreach ($quran_page_tr as $aya_tr)
                {
                        if ($aya_ar['index'] == $aya_tr['index'])
                        {
                            if ($aya_ar['aya'] == '1')
                            {
                                foreach ($quran_suras as $sura) {
                                    if($aya_ar['sura'] == $sura['quran_suras_sura_index'])
                                    {
                                        echo "<div class='suraName noselect'>سورة " . $sura['quran_suras_sura_name'] . "</div>";
                                    }
                                }
                            }

                            echo "&nbsp;<span class='quran-text-ar' data-index='".$aya_ar['index']."' data-aya='".$aya_ar['sura'].":".$aya_ar['aya']."'>".$aya_ar['text'] . '<small>﴿'.$aya_ar['aya'].'﴾</small></span>';
                        }
                }
            }
            echo "</div></div>";

            echo "<div class='quran_tr_text pull-left'>";
            echo "<div class='quran_textbox_info'><div class='pull-right'>".$quran_suras[$quran_page_ar[0]['sura']-1]['quran_suras_sura_name']."</div>";
            echo    "<div class='pull-left'>".$index."</div><div class='clearfix'></div></div>";
            echo "<div class='quran_ar_content'>";
            foreach($quran_page_ar as $aya_ar)
            {
                foreach ($quran_page_tr as $aya_tr)
                {
                        if ($aya_ar['index'] == $aya_tr['index'])
                        {
                            if ($aya_ar['aya'] == '1')
                            {
                                foreach ($quran_suras as $sura)
                                {
                                    if($aya_ar['sura'] == $sura['quran_suras_sura_index'])
                                    {
                                        echo "<div class='suraNameTr noselect'>سوره " . $sura['quran_suras_sura_name'] . "</div>";
                                    }
                                }
                            }
                            echo "&nbsp;<span  class='quran-text-tr' data-index='".$aya_ar['index']."'  data-aya='".$aya_ar['sura'].":".$aya_ar['aya']."'>".$aya_tr['text'] . '<small>('.$aya_tr['aya'].')</small></span>';
                        }
                }
            }
            echo "</div></div>";
            echo "<div class='clearfix'></div></div>";
        }
        else
        {
            echo 'Invalid !';
        }
    }
    else
    {
        null;
    }

?>
