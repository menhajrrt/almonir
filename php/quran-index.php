<?php

$date1 = new DateTime("2016-10-21");
$date2 = new DateTime();
$quran_pages = 604;

$diff = $date2->diff($date1)->format("%a");
$countRepeat = ceil($diff/$quran_pages);

if ($diff > $quran_pages)
{
  $quran_page = ($countRepeat*$quran_pages) - $diff;
}
else
{
  $quran_page = $diff;
}

if ($quran_page == 0)
{
  $quran_page = 1;
}

$returnArray = array(
    'quran_page'  =>  1
  );

echo json_encode($returnArray);
