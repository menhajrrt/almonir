<?php
class Quran
{
    protected $page_namber;

    function getIndex($val)
    {
        $this->page_namber = $val;
    }

    function getAllSuras()
    {
        include '../config.php';

        $sql = $db_con->prepare("SELECT * FROM `quran_suras`");

        $sql->execute();
        return $sql->fetchAll();
    }

    function getDatas($page_namber)
    {
        include '../config.php';

        $sql = $db_con->prepare("SELECT * FROM `quran_data` WHERE `quran_pages_page_index`=:quran_pages_page_index");
        $sql->bindParam(':quran_pages_page_index', $page_namber);

        $sql->execute();
        return $sql->fetchAll();
    }

    function getTexts($sura, $aya)
    {
        include '../config.php';

        $sql = $db_con->prepare("SELECT * FROM `quran_text` WHERE `sura`=:sura AND `aya`=:aya");
        $sql->bindParam(':sura',$sura);
        $sql->bindParam(':aya',$aya);

        $sql->execute();
        return $sql->fetchAll();
    }

    /**
     *  Get text of ayas $fromIndex to $toIndex
     *  $fromIndex and $toIndex are numbers of aya in quran(not sura)
     */
    function getTextsPage($fromIndex, $toIndex)
    {
        include '../config.php';

        $sql = $db_con->prepare("SELECT * FROM `quran_text` WHERE `index` BETWEEN $fromIndex and $toIndex");

        $sql->execute();
        return $sql->fetchAll();
    }

    /**
     *  Get translate of ayas $fromIndex to $toIndex
     *  $fromIndex and $toIndex are numbers of aya in quran(not sura)
     */
    function getTextsPageTr($trTable, $fromIndex, $toIndex)
    {
        include '../config.php';

        $sql = $db_con->prepare("SELECT * FROM $trTable WHERE `index` BETWEEN $fromIndex and $toIndex");

        $sql->execute();
        return $sql->fetchAll();
    }


    /**
     *
     *  Get every Suras page number and return it
     */
    public function getSurasIndex($sura)
    {
      include '../config.php';
      $sura = intval($sura);

      $sql = $db_con->prepare("SELECT * FROM `quran_index` WHERE `sura`=:sura");
      $sql->bindParam(':sura', $sura);

      $sql->execute();
      $suraMeta = $sql->fetch(PDO::FETCH_ASSOC);
      return $suraMeta['page'];
    }

    /**
    *
    *   Tafsir Section
    */
    public function getTafsir($aya_index, $tafsir)
    {
      include '../config.php';
      $aya_index = intval($aya_index);

      $sql = $db_con->prepare("SELECT * FROM $tafsir WHERE `aya`=:aya");
      $sql->bindParam(':aya', $aya_index);

      $sql->execute();
      return $sql->fetchAll();
    }
}
